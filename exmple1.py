import serial
from CenterLib import*
import Tkinter
import numpy
import matplotlib.pyplot as plt
from drawnow import*

HumD = []

tempC =[]
cnt =0
arduinoData = serial.Serial('com5', 9600)

def makeFig():
  
    plt.title('BIEU DO HIEN THI NHIET DO VA DO AM')
    plt.grid(True)
    plt.ylabel('Nhiet do C va Do am %')
    plt.plot(tempC, 'r-',label = 'Nhiet do C')
    plt.legend(loc = 'upper left')
    plt2=plt.twinx()
    plt2.plot(HumD, 'b-',label='Do Am %')
    plt2.legend(loc='upper right')

while True:
    while(arduinoData.inWaiting()==0):
        pass
    arduinoString = arduinoData.readline()
    dataArray = arduinoString.split(',')
    Hum  = float(dataArray[0])
    Temp = float( dataArray[1])
    HumD.append(Hum)
    tempC.append(Temp)
    drawnow(makeFig)
    plt.pause(.000001)
    cnt = cnt+1
    if (cnt>50):
       tempC.pop(0)
       HumD.pop(0)



