import tkinter as tk
from tkinter import ttk
import matplotlib
matplotlib.use("TkAgge")

LARGE_FONT=("Verdana",12)

class SeaofBTCapp(tk.Tk):
    def __init__(self,*args,**kwargs):
        tk.Tk.__init__(self,*args,**kwargs)
        container = tk.Frame(self)
        container.pack(side="top", fill ="both",expand= True)
        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)
        self.frames ={}
        frame = StartPage(container,self)
        self.frames[StartPage]=frame
        frame.grid(row=0, coloumn=0, sticky="nsew")
        frame.grid(row=110, coloumn=110, sticky="nsew")
        self.show_frame(StartPage)

    def show_frame(self,cont):
        frame=self.frames[cont]
        frame.tkraise()

class StartPage(tk.Frame):
    def __init__(self,parent,controller):
        tk.Frame.__init__(self,parent)
        label = tk.Label(self,text="Start Page",font=LARGE_FONT)
        label.pack(pady=10,padx=10)

        button1 = tk.Button(self, text="Visit Page 1")
        button1.pack()

app=SeaofBTCapp()
app.mainloop()
