import tkinter as tk
from tkinter import ttk
# https://www.youtube.com/watch?v=KHjtFNM3Cbk&t=5052s

win = tk.Tk()
win.title("Python GUI")

alabel = ttk.Label(win, text="A label")
alabel.grid(column = 0, row =0)

def clickMe():
    action.configure(text = 'Hell' + name.get())

ttk.Label(win, text = " Enter a name:").grid(column = 0, row = 0)
name = tk.StringVar()
nameEntered = ttk.Entry(win, width = 12, textvariable = name)
nameEntered.grid(column = 0, row =1)

action = ttk.Button(win, text="Click Me !", command = clickMe)
action.grid(column = 2, row =1)

ttk.Label(win, text ="Choose a number:").grid(column = 1, row = 0)
number = tk.StringVar()
numberChosen = ttk.Combobox(win, width = 12, textvariable = number, state = 'readonly')
numberChosen['values'] = (1,2,4,42,100)
numberChosen.grid(column = 1, row =1)
numberChosen.current(0)

# Create check button
chVarDis = tk.IntVar()

check1 = tk.Checkbutton(win, text = "Disabled", variable = chVarDis, state = 'disabled')
check1.select()
check1.grid(column =0 , row = 4, sticky = tk.W)
chVarUn = tk.IntVar()
check2 = tk.Checkbutton(win, text ="UnChecked", variable = chVarUn)
check2.deselect()
check2.grid(column = 1, row = 4, sticky = tk.W)

chVarEn = tk.IntVar()
check3 = tk.Checkbutton(win, text="Enabled", variable = chVarEn)
check3.select()
check3.grid(column = 2, row = 4, sticky = tk.W)


nameEntered.focus()

win.mainloop()