from tkinter import*
import random
import time
import os, sys


#https://www.youtube.com/watch?v=hqC9tioGCi0

root = Tk()
root.geometry("1200x600+0+0")
root.title("Resurant Management Systems")

text_Input = StringVar()
operator=" "

Tops = Frame(root, width =1200, height=50, bg = "powder blue", relief=SUNKEN)
Tops.pack(side=TOP)

f1 = Frame(root, width =800,height=700,relief=SUNKEN)
f1.pack(side=LEFT)

f2 = Frame(root, width =300,height=700, bg = "powder blue", relief=SUNKEN)
f2.pack(side=RIGHT)

#==========================================time==========================================================================
localtime = time.asctime(time.localtime(time.time()))
#===========================================Info==========================================================================
lblInfo = Label(Tops,font = ('arial',40,'bold'),text= "Resurant Management Systems", fg= "Steel Blue",bd=10,anchor ='w')
lblInfo.grid(column =0, row =0)

lbltime = Label(Tops,font = ('arial',20,'bold'),text=localtime, fg= "Steel Blue",bd=10,anchor ='w')
lbltime.grid(column =0, row =1)
#===========================================Caculator=====================================================================
def btnClick(numbers):
    global operator
    operator =operator +str(numbers)
    text_Input.set(operator)

def btnClearDisplay():
    global operator
    operator = ""
    text_Input.set("")
def btnEqualsInput():
    global operator
    sumup = str(eval(operator))
    text_Input.set(sumup)
    operator = ""
def Ref():
    x = random.randint(10908,500876)
    randomRef = str(x)
    rand.set(randomRef)
    
    CoF=float(Fries.get())
    CoD=float(Drinks.get())
    CoFilet=float(Filler.get())
    CoBurger =float(Burger.get())
    CoChicBurger = float(Chicken.get())
    CoCheese = float(Cheese.get())

    CostofFries = CoF * 0.99
    CostofDrinks = CoD * 1.00
    CostofFilet = CoFilet * 2.99
    CostofBurger = CoBurger * 2.87
    CostChicBurger = CoChicBurger * 2.89
    CostCheese = CoCheese * 2.89

    CostofMeal = "£", str('%.2f'%(CostofFries + CostofDrinks+CostofFilet + CostofBurger + CostChicBurger + CostCheese))
    
    PayTax = ((CostofFries + CostofDrinks + CostofFilet + CostofBurger + CostChicBurger + CostCheese) * 0.2)
    
    TotalCost = (CostofFries + CostofDrinks + CostofFilet + CostofBurger + CostChicBurger + CostCheese)

    Ser_Change = ((CostofFries + CostofDrinks + CostofFilet + CostofBurger + CostChicBurger + CostCheese)/99)

    SevriceCost = "£", str('%.2f'%Ser_Change)
    
    OverAllCost = "£", str('%.2f'%(PayTax + TotalCost + Ser_Change))

    PaidTax = "£", str('%.2f'%PayTax)
    
    Service.set(SevriceCost)
    
    Cost.set(CostofMeal)
    StateTax.set(PaidTax)
    SubTotal.set(CostofMeal)
    Total.set(OverAllCost)





def qExit():
    root.destroy()
def Reset():
    rand.set("")
    Fries.set("")
    Burger.set("")
    Filler.set("")
    Chicken.set("")
    Cheese.set("")
    Drinks.set("")
    Cost.set("")
    Service.set("")
    StateTax.set("")
    SubTotal.set("")
    Total.set("")

txtDisplay=Entry(f2,font=('arial',25), textvariable=text_Input, bd=10,insertwidth=40, bg="powder blue",justify='right')
txtDisplay.grid(columnspan=4)

btn7 = Button(f2,padx=16,pady=16,bd=8, fg="black",font=('arial',20,'bold'),text="7", bg="powder blue", command=lambda:btnClick(7)).grid(column=0,row=2)
btn8 = Button(f2,padx=16,pady=16,bd=8, fg="black",font=('arial',20,'bold'),text="8", bg="powder blue", command=lambda:btnClick(8)).grid(column=1,row=2)
btn9 = Button(f2,padx=16,pady=16,bd=8, fg="black",font=('arial',20,'bold'),text="9", bg="powder blue", command=lambda:btnClick(9)).grid(column=2,row=2)
Addition = Button(f2,padx=16,pady=16,bd=8, fg="black",font=('arial',20,'bold'),text="+", bg="powder blue", command=lambda:btnClick("+")).grid(column=3,row=2)
#=============================================================================================================================================================

btn4 = Button(f2,padx=16,pady=16,bd=8, fg="black",font=('arial',20,'bold'),text="4", bg="powder blue", command=lambda:btnClick(4)).grid(column=0,row=3)
btn5 = Button(f2,padx=16,pady=16,bd=8, fg="black",font=('arial',20,'bold'),text="5", bg="powder blue", command=lambda:btnClick(5)).grid(column=1,row=3)
btn6 = Button(f2,padx=16,pady=16,bd=8, fg="black",font=('arial',20,'bold'),text="6", bg="powder blue", command=lambda:btnClick(6)).grid(column=2,row=3)
Subtraction = Button(f2,padx=16,pady=16,bd=8, fg="black",font=('arial',20,'bold'),text=" -", bg="powder blue", command=lambda:btnClick("-")).grid(column=3,row=3)
#=============================================================================================================================================================

btn1 = Button(f2,padx=16,pady=16,bd=8, fg="black",font=('arial',20,'bold'),text="1", bg="powder blue", command=lambda:btnClick(1)).grid(column=0,row=4)
btn2 = Button(f2,padx=16,pady=16,bd=8, fg="black",font=('arial',20,'bold'),text="2", bg="powder blue", command=lambda:btnClick(2)).grid(column=1,row=4)
btn3 = Button(f2,padx=16,pady=16,bd=8, fg="black",font=('arial',20,'bold'),text="3", bg="powder blue", command=lambda:btnClick(3)).grid(column=2,row=4)
Multiply = Button(f2,padx=16,pady=16,bd=8, fg="black",font=('arial',20,'bold'),text="x", bg="powder blue", command=lambda:btnClick("x")).grid(column=3,row=4)
#=============================================================================================================================================================
btn0 = Button(f2,padx=16,pady=16,bd=8, fg="black",font=('arial',20,'bold'),text="0", bg="powder blue", command=lambda:btnClick(0)).grid(column=0,row=5)
btnClear = Button(f2,padx=16,pady=16,bd=8, fg="black",font=('arial',20,'bold'),text="C", bg="powder blue",command=btnClearDisplay).grid(column=1,row=5)
btnEquals = Button(f2,padx=16,pady=16,bd=8, fg="black",font=('arial',20,'bold'),text="=", bg="powder blue",command=btnEqualsInput).grid(column=2,row=5)
Division = Button(f2,padx=16,pady=16,bd=8, fg="black",font=('arial',20,'bold'),text="./", bg="powder blue", command=lambda:btnClick("/")).grid(column=3,row=5)
#==============================================Restaurant Info 1=======================================================================================================
rand= StringVar()
Fries = StringVar()
Burger = StringVar()
Filler = StringVar()
Chicken= StringVar()
Cheese = StringVar()
Drinks = StringVar()
Cost = StringVar()
Service = StringVar()
StateTax = StringVar()
SubTotal = StringVar()
Total = StringVar()

lblReference = Label(f1,font=('arial',16,'bold'), text = "Reference",bd=16, anchor='w')
lblReference.grid(column=0,row =0)
txtReference = Entry(f1,font=('arial',16,'bold'), textvariable=rand,bd=10, insertwidth=4,bg="powder blue",justify='right')
txtReference.grid(column=1,row =0)

lblLage_Fries = Label(f1,font=('arial',16,'bold'), text = "Lage_Fries",bd=16, anchor='w')
lblLage_Fries.grid(column=0,row =2)
txtLage_Fries = Entry(f1,font=('arial',16,'bold'), textvariable=Fries,bd=10, insertwidth=4,bg="powder blue",justify='right')
txtLage_Fries.grid(column=1,row =2)

lblBurger = Label(f1,font=('arial',16,'bold'), text = "Burger Meal",bd=16, anchor='w')
lblBurger.grid(column=0,row =3)
txtBurger = Entry(f1,font=('arial',16,'bold'), textvariable=Burger,bd=10, insertwidth=4,bg="powder blue",justify='right')
txtBurger.grid(column=1,row =3)

lblFiler = Label(f1,font=('arial',16,'bold'), text = "Filler_o_Meal", bd=16, anchor='w')
lblFiler.grid(column=0,row =4)
txtFiler = Entry(f1,font=('arial',16,'bold'), textvariable=Filler,bd=10, insertwidth=4,bg="powder blue",justify='right')
txtFiler.grid(column=1,row =4)

lblChicken = Label(f1,font=('arial',16,'bold'), text = "Chicken Meal",bd=16, anchor='w')
lblChicken.grid(column=0,row =5)
txtChicken = Entry(f1,font=('arial',16,'bold'), textvariable=Chicken,bd=10, insertwidth=4,bg="powder blue",justify='right')
txtChicken.grid(column=1,row =5)

lblCheese = Label(f1,font=('arial',16,'bold'), text = "Cheese Meal",bd=16, anchor='w')
lblCheese.grid(column=0,row =6)
txtCheese = Entry(f1,font=('arial',16,'bold'), textvariable=Cheese,bd=10, insertwidth=4,bg="powder blue",justify='right')
txtCheese.grid(column=1,row =6)
#==============================================Restaurant Info 2=======================================================================================================
lblDrinks = Label(f1,font=('arial',16,'bold'), text = "Drinks",bd=16, anchor='w')
lblDrinks.grid(column=2,row =0)
txtDrinks = Entry(f1,font=('arial',16,'bold'), textvariable=Drinks,bd=10, insertwidth=4,bg="powder blue",justify='right')
txtDrinks.grid(column=3,row =0)

lblCost = Label(f1,font=('arial',16,'bold'), text = "Cost VND",bd=16, anchor='w')
lblCost.grid(column=2,row =2)
txtCost = Entry(f1,font=('arial',16,'bold'), textvariable=Cost,bd=10, insertwidth=4,bg="powder blue",justify='right')
txtCost.grid(column=3,row =2)

lblService = Label(f1,font=('arial',16,'bold'), text = "Service Change",bd=16, anchor='w')
lblService.grid(column=2,row =3)
txtService = Entry(f1,font=('arial',16,'bold'), textvariable=Service,bd=10, insertwidth=4,bg="powder blue",justify='right')
txtService.grid(column=3,row =3)

lblStateTax = Label(f1,font=('arial',16,'bold'), text = "State Tax",bd=16, anchor='w')
lblStateTax.grid(column=2,row =4)
txtStateTax = Entry(f1,font=('arial',16,'bold'), textvariable=StateTax,bd=10, insertwidth=4,bg="powder blue",justify='right')
txtStateTax.grid(column=3,row =4)

lblSubTotal = Label(f1,font=('arial',16,'bold'), text = "Sub Total",bd=16, anchor='w')
lblSubTotal.grid(column=2,row =5)
txtSubTotal = Entry(f1,font=('arial',16,'bold'), textvariable=SubTotal,bd=10, insertwidth=4,bg="powder blue",justify='right')
txtSubTotal.grid(column=3,row =5)

lblTotal = Label(f1,font=('arial',16,'bold'), text = "Total",bd=16, anchor='w')
lblTotal.grid(column=2,row =6)
txtTotal = Entry(f1,font=('arial',16,'bold'), textvariable=Total,bd=10, insertwidth=4,bg="powder blue",justify='right')
txtTotal.grid(column=3,row =6)


#===================================================Buttons================================================================================
btnTotal = Button(f1,padx =16, pady=8,bd=16, fg="Black", font=('arial', 16,'bold'),width=10, text="Total", bg="powder blue",command = Ref).grid(column=1, row =8)
btnReset = Button(f1,padx =16, pady=8,bd=16, fg="Black", font=('arial', 16,'bold'),width=10, text="Reset", bg="powder blue",command = Reset).grid(column=2, row =8)
btnExit = Button(f1,padx =16, pady=8,bd=16, fg="Black", font=('arial', 16,'bold'),width=10, text="Exit", bg="powder blue",command = qExit).grid(column=3, row =8)




root.iconbitmap(r'E:\python\project\learningGui\chicken.ico')
root.mainloop()