import tkinter as tk
from tkinter import ttk
from tkinter import scrolledtext
from tkinter import Menu
from tkinter import messagebox as mBox
from tkinter import Spinbox
from tkinter import*
import random
import time
import OOP_ToolTip as tt
from threading import Thread
from openpyxl import load_workbook
from tkinter import filedialog
import openpyxl

# https://www.youtube.com/watch?v=KHjtFNM3Cbk&t=5052s

GLOBAL_CONST = 42
operator=""


win = tk.Tk()
win.title("PHẦN MÊM QUẢN LÝ CHICKEN FARM")
tabControl = ttk.Notebook(win)
tabl1 = ttk.Frame(tabControl)
tabControl.add(tabl1,text="THÔNG SỐ NHẬP VÀO")
tabl2 = ttk.Frame(tabControl)
tabControl.add(tabl2, text="THÔNG SỐ TÍNH TOÁN")

tabControl.pack(expand=1, fill="both")

#======================================TIME=============================
localtime = time.asctime(time.localtime(time.time()))
lbltime = Label(tabl1,font = ('arial',20,'bold'),text=localtime, fg= "Steel Blue",bd=10,anchor ='w')
lbltime.grid(column =0, row =0)
#=============================================================================
monty = ttk.LabelFrame(tabl1, text ='BẢNG SỐ LIỆU')
monty.grid(column =0, row = 1, ipadx =10)
tieude=ttk.LabelFrame(monty)
tieude.grid(column = 0,row=0, ipadx=50)
HienThi=ttk.LabelFrame(monty)
HienThi.grid(column = 0 , row = 1, ipadx=20)
TP=ttk.LabelFrame(tieude)
TP.grid(column =0, row =0)
NL=ttk.LabelFrame(tieude)
NL.grid(column = 1, row=0)
SLDV=ttk.LabelFrame(tieude)
SLDV.grid(column = 2, row= 0)
GT=ttk.LabelFrame(tieude)
GT.grid(column=3, row =0)
DV=ttk.LabelFrame(tieude)
DV.grid(column = 4, row =0)
option1 = ttk.LabelFrame(monty)
option1.grid(column =0, row=2)

# ======================================Khai báo biến=========================
operator=""
name= StringVar()
money = StringVar()
Tpname = ["CÁM", "THUỐC", "ĐIỆN", "VT SẮT","VT ĐIỆN","VT KHÁC","TRỨNG GÀ",
"GÀ GIỐNG","GÀ THẢI"]
number = tk.StringVar()
numberdv = ["Kg","g","Tấn","Bao" ,"VND","lít","cái","chiếc","Quả","Con"]
filePath = StringVar()
#============================File Excel ======================================
def fileLink():
    try:
        fileNameLink = filedialog.askopenfilename(filetypes = (("File","*.xlsx"), 
        (" All Files","*.*")))
        filePath.set(fileNameLink)
        wb = load_workbook(filePath.get())
    except IOError:
        mBox.showerror('THÔNG BÁO','Không tìm thấy file')


labelsFrame = ttk.LabelFrame(monty, text ='Nhập đường dẫntới File Excel:')
labelsFrame.grid(column = 0, row =7,padx=10, pady=20)
ttk.Label(labelsFrame, text="Link:").grid(column =0, row =0 , sticky = tk.W)
link=ttk.Entry(labelsFrame,width=60, textvariable = filePath).grid(column =1, row=0)
btnlink =ttk.Button(labelsFrame, width = 20, text="Chọn Đường Dẫn",command = fileLink)
btnlink.grid(column =2, row=0)

#=============================Fuction=========================================
def isfloat(value):
        try:
            float(value)
            return True
        except ValueError:
            return False
def clickOK():   
        if(money.get()==""):
            mBox.showwarning('THÔNG BÁO', 'VUI LÒNG NHẬP GIÁ TIỀN')  
        else:
            if(isfloat(money.get())):
                moneyfl= float(money.get())
                CoPin =  float(spin.get())
                CosNumber = float(number.get())
                CostNumber = str(CoPin*CosNumber)
                CostGT = moneyfl*float(CostNumber)
                CotGT =str('%.3f'%CostGT)
                value1 = numberChosenTP.get() +"    "+ name.get() +"    "+ CostNumber +"   "+ numberChosendv.get() +"    "+CotGT+"    "+"VND"
                    # value2 = number.get()
                if (name.get() ==""):
                    mBox.showerror('THÔNG BÁO', 'VUI LÒNG NHẬP TÊN SẢN PHẨM')
                else: 
                    scr.insert(tk.INSERT,value1 + '\n')

                    if(numberChosenTP.get()=="CÁM"):
                        wb = load_workbook(filePath.get())
                        sheetWc = wb.get_sheet_by_name('Cám')
                        columsc = sheetWc['B']
                        for ic in columsc:
                            if ic.value == None:
                                xc= float(ic.row)
                                sheetWc.cell(row = xc, column = 2).value = name.get()
                                sheetWc.cell(row = xc, column = 3).value = CostNumber 
                                sheetWc.cell(row = xc, column = 4).value = CotGT
                                wb.save(filePath.get())
                                break                       
                    elif(numberChosenTP.get()=="THUỐC"):
                        wb = load_workbook(filePath.get())                     
                        sheetWt = wb.get_sheet_by_name('Thuốc')
                        columst = sheetWt['B']
                        for it in columst:
                            if it.value == None:
                                xt= float(it.row)
                                sheetWt.cell(row = xt, column = 2).value = name.get()
                                sheetWt.cell(row = xt, column = 3).value = CostNumber 
                                sheetWt.cell(row = xt, column = 4).value = CotGT
                                wb.save(filePath.get())
                                break
                                   
                    elif(numberChosenTP.get()=="ĐIỆN"):
                        wb = load_workbook(filePath.get())                  
                        sheetWd = wb.get_sheet_by_name('Điện')
                        columsd = sheetWd['B']
                        for id in columsd:
                            if id.value == None:
                                xd= float(id.row)
                                sheetWd.cell(row = xd, column = 2).value = name.get()
                                sheetWd.cell(row = xd, column = 3).value = CostNumber 
                                sheetWd.cell(row = xd, column = 4).value = CotGT
                                wb.save(filePath.get())
                                break
                                       
                    elif(numberChosenTP.get()=="VT SẮT"):
                        wb = load_workbook(filePath.get())                     
                        sheetWvs = wb.get_sheet_by_name('VT.Sắt')
                        columsvs = sheetWvs['B']
                        for ivs in columsvs:
                            if ivs.value == None:
                                xvs= float(ivs.row)
                                sheetWvs.cell(row = xvs, column = 2).value = name.get()
                                sheetWvs.cell(row = xvs, column = 3).value = CostNumber 
                                sheetWvs.cell(row = xvs, column = 4).value = CotGT
                                wb.save(filePath.get())
                                break
                     
                    elif(numberChosenTP.get()=="VT ĐIỆN"):
                        wb = load_workbook(filePath.get())                     
                        sheetWtd = wb.get_sheet_by_name('VT.Điện')
                        columstd = sheetWtd['B']
                        for itd in columstd:
                            if itd.value == None:
                                xtd= float(itd.row)
                                sheetWtd.cell(row = xtd, column = 2).value = name.get()
                                sheetWtd.cell(row = xtd, column = 3).value = CostNumber 
                                sheetWtd.cell(row = xtd, column = 4).value = CotGT
                                wb.save(filePath.get())
                                break
                        
                    elif(numberChosenTP.get()=="VT KHÁC"):
                        wb = load_workbook(filePath.get())                       
                        sheetWtk = wb.get_sheet_by_name('VT.Khác')
                        columstk = sheetWtk['B']
                        for itk in columstk:
                            if itk.value == None:
                                xtk= float(itk.row)
                                sheetWtk.cell(row = xtk, column = 2).value = name.get()
                                sheetWtk.cell(row = xtk, column = 3).value = CostNumber 
                                sheetWtk.cell(row = xtk, column = 4).value = CotGT
                                wb.save(filePath.get())
                                break
            
                    elif(numberChosenTP.get()=="TRỨNG GÀ"):
                        wb = load_workbook(filePath.get())                  
                        sheetWtg = wb.get_sheet_by_name('Trứng.Gà')
                        columstg = sheetWtg['B']
                        for itg in columstg:
                            if itg.value == None:
                                xtg= float(itg.row)
                                sheetWtg.cell(row = xtg, column = 2).value = name.get()
                                sheetWtg.cell(row = xtg, column = 3).value = CostNumber 
                                sheetWtg.cell(row = xtg, column = 4).value = CotGT
                                wb.save(filePath.get())
                                break
                                          
                    elif(numberChosenTP.get()=="GÀ GIỐNG"):
                        wb = load_workbook(filePath.get())                    
                        sheetWgg = wb.get_sheet_by_name('Gà.Giống')
                        columsgg = sheetWgg['B']
                        for igg in columsgg:
                            if igg.value == None:
                                xgg= float(igg.row)
                                sheetWgg.cell(row = xgg, column = 2).value = name.get()
                                sheetWgg.cell(row = xgg, column = 3).value = CostNumber 
                                sheetWgg.cell(row = xgg, column = 4).value = CotGT
                                wb.save(filePath.get())
                                break                     
                    
                    elif(numberChosenTP.get()=="GÀ THẢI"):
                        wb = load_workbook(filePath.get())
                        sheetWgt = wb.get_sheet_by_name('Gà.Thải')
                        columsgt = sheetWgt['B']
                        for igt in columsgt:
                            if igt.value == None:
                                xgt= float(igt.row)
                                sheetWgt.cell(row = xgt, column = 2).value = name.get()
                                sheetWgt.cell(row = xgt, column = 3).value = CostNumber 
                                sheetWgt.cell(row = xgt, column = 4).value = CotGT
                                wb.save(filePath.get())
                                break

            else: mBox.showerror('THÔNG BÁO', 'VUI LÒNG NHẬP SỐ VÀO GIÁ TIỀN')
        

def clickCancel():
    global operator
    operator = ""
    name.set("")
# RadioButton CallsBack
def radCall():
    radsel = radVar.get()
    if radsel == 0: win.configure(background=color[0])
    elif radsel == 1: win.configure(background = color[1])
    elif radsel == 2: win.configure(background = color[2])
# GUI callBack function
def checkCallback(*ignoredArgs):
    if chVarUn.get(): check3.configure(state='disabled')
    else: check3.configure(state='normal')
    if chVarEn.get(): check2.configure(state='disabled')
    else: check2.configure(state='normal')
# Exit Gui cleanly
def _quit():
    win.quit()
    win.destroy()
    exit

def methodInAThread(self):
    print('Hi, how are you?')
# Display a Message Box
def _msgBox():
    # CallBack function
    mBox.showinfo('PHẦN MÊM QUẢN LÝ CHICKEN FARM',
     'Sử dụng cho chicken Farm của Hoàn:\n Khởi tạo năm 2018')

# ==================================== Info===================================
ttk.Label(TP, text = " Thành Phần:").grid(column = 0, row = 0)
numberChosenTP = ttk.Combobox(TP, width = 10, values=Tpname, state = 'readonly')
numberChosenTP.grid(column = 0, row =1)
numberChosenTP.current(0)

ttk.Label(NL, text = " Tên Nguyên Liệu:").grid(column = 1, row = 0)
nameEntered = ttk.Entry(NL, width = 30, textvariable = name)
nameEntered.grid(column = 1, row =1)

action = ttk.Button(tieude, text="OK", command = clickOK)
action.grid(column = 8, row =0)

actionCancel = ttk.Button(tieude, text="Cancel", command = clickCancel)
actionCancel.grid(column = 9, row =0)

ttk.Label(SLDV, text ="Số Lượng Đầu Vào:").grid(column = 2, row = 0)
numberChosen = ttk.Combobox(SLDV, width = 6, textvariable = number, state = 'readonly')
numberChosen['values'] = (1,5,10,15,20,25,50,100,1000,2000,3000,4000,5000)
numberChosen.grid(column = 2, row =1)
numberChosen.current(0)

ttk.Label(GT, text = " Giá Thành(VND):").grid(column = 3, row = 0)
nameMoney = ttk.Entry(GT, width = 12, textvariable = money)
nameMoney.grid(column = 3, row =1)

# Create đơn vị
ttk.Label(DV, text ="Đơn Vị:").grid(column = 6, row = 0)
numberChosendv = ttk.Combobox(DV, width = 6, values=numberdv,
 state = 'readonly')
#numberChosendv['values'] = (1,2,3,4,5)
numberChosendv.grid(column =6, row =1)
numberChosendv.current(0)

# Create check button
chVarDis = tk.IntVar()

check1 = tk.Checkbutton(tieude, text = "Disabled", variable = chVarDis,
 state='disabled')
check1.select()
check1.grid(column =0 , row = 4, sticky = tk.W)
chVarUn = tk.IntVar()
check2 = tk.Checkbutton(tieude, text ="UnChecked", variable = chVarUn)
check2.deselect()
check2.grid(column = 1, row = 4, sticky = tk.W)

chVarEn = tk.IntVar()
check3 = tk.Checkbutton(tieude, text="Enabled", variable = chVarEn)
check3.select()
check3.grid(column = 2, row = 4, sticky = tk.W)

# Create Spinbox
spin = Spinbox(HienThi, from_= 0, to=10, width=5, bd=8)
spin.grid(column = 5, row=5, sticky = tk.E)

tt.createToolTip(spin, 'Lựa chọn số này.')

## chVarDis.trace('w', lambda unused0, unused1,unused2: checkCallback())
chVarUn.trace('w', lambda unused0, unused1,unused2: checkCallback())
chVarEn.trace('w', lambda unused0, unused1,unused2: checkCallback())
# Radio Button Globals

color = ["Blue", "Gold", "Red"]

# Creeate three RadioButtons using one variable

radVar = tk.IntVar()
# Next we are selecting a non-exitting index value for radVar

radVar.set(99)

# Now we are creating all three RadioButton widgets winthin one loop
for col in range(3):
    curRad = 'rad' + str(col)
    curRad = tk.Radiobutton(option1, text = color[col], variable = radVar, value = col, command = radCall)
    curRad.grid(column = col, row =6, sticky=tk.W)

# Using a scrolled Text control
scrolW = 60
scrolH = 20

scr = scrolledtext.ScrolledText(HienThi, width=scrolW, height = scrolH, wrap = tk.WORD)

scr.grid(column = 0, row=5, columnspan = 3)


# Create a Menu bar
menuBar = Menu(win)
win.config(menu=menuBar)

fileMenu = Menu(menuBar, tearoff =0)
fileMenu.add_command(label="New")
fileMenu.add_separator()
fileMenu.add_command(label="Exit", command=_quit)
menuBar.add_cascade(label="File",menu=fileMenu)

# Add another Menu to the Menu Bar and on item
HelpMenu = Menu(menuBar, tearoff=0)
HelpMenu.add_command(label="About", command=_msgBox)
HelpMenu.add_separator()
HelpMenu.add_command(label="Help")
menuBar.add_cascade(label="Help",menu=HelpMenu)

win.iconbitmap(r'E:\python\project\learningGui\chicken.ico')
chicken_photo = PhotoImage(file='E:\python\project\learningGui\chicken6.png')
imgeChicK = Label(HienThi, image = chicken_photo).grid(column=8,row =5, sticky =tk.SE)

nameEntered.focus()

win.mainloop()