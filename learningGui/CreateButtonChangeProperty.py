import tkinter as tk
from tkinter import ttk

win = tk.Tk()
win.title("Python GUI")

alabel = ttk.Label(win, text = "A Label")
alabel.grid(column = 0, row = 0)

def clickMe():
    action.configure(text="** I have  been Clicked!**")
    alabel.configure(foreground='red')
    alabel.configure(text = 'A Red Label')
action = ttk.Button(win, text="ClickMe!", command=clickMe)
action.grid(column = 1, row = 0)

win.mainloop()