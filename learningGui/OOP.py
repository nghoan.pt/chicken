import tkinter as tk
from tkinter import ttk
from tkinter import scrolledtext
from tkinter import Menu
from tkinter import messagebox as mBox
from tkinter import Spinbox
import OOP_ToolTip as tt
from threading import Thread
from time import sleep

# https://www.youtube.com/watch?v=KHjtFNM3Cbk&t=5052s

GLOBAL_CONST = 42
class OPP():
    def __init__(self):
        self.win = tk.Tk()
        tt.createToolTip(self.win, "Hello GUI")
        self.win.title("Python GUI")
        self.createWidgets()
    def createThread(self):
        runT = Thread(target=self.methodInAThread)
        runT.start()
        print(runT)
    def clickMe(self):
        self.action.configure(text = 'Hell' + self.name.get())
       # for idx in range(10):
          #  sleep(5)
          #  self.scr.insert(tk.INSERT, str(idx) + '\n')
          self.createThread()

    def _spin(self):
        value = self.spin.get()
        print(value)
        self.scr.insert(tk.INSERT,value + '\n')
    def checkCallback(self, *ignoredArgs):
        if self.chVarUn.get(): self.check3.configure(state='disabled')
        else: self.check3.configure(state='normal')
        if self.chVarEn.get(): self.check2.configure(state='disabled')
        else: self.check2.configure(state='normal')

    def radCall(self):
        radsel = self.radVar.get()
        if radsel == 0:   self.monty.configure(background=color[0])
        elif radsel == 1: self.monty.configure(background = color[1])
        elif radsel == 2: self.monty.configure(background = color[2])

    def createWidgets(self):
        tabControl = ttk.Notebook(self.win)
        tabl1 = ttk.Frame(tabControl)
        tabControl.add(tabl1,text="THÔNG SỐ NHẬP VÀO")
        tabl2 = ttk.Frame(tabControl)
        tabControl.add(tabl2, text="THÔNG SỐ TÍNH TOÁN")

        tabControl.pack(expand=1, fill="both")
    def _quit(self):
        self.win.quit()
        self.win.destroy()
        exit

    def methodInAThread(self):
        print('Hi, how are you?')

win = tk.Tk()
win.title("PHẦN MÊM QUẢN LÝ CHICKEN FARM")
#win.geometry("1400x900")
#win.overrideredirect(True)
#win.geometry("{0}x{1}+0+0".format(win.winfo_screenwidth(),win.winfo_screenmmheight()))
# Create tabcontrol

monty = ttk.LabelFrame(tabl1, text ='Monty Python')
monty.grid(column =0, row = 0, padx =10, pady =10)

alabel = ttk.Label(monty, text="A label")
alabel.grid(column = 0, row =0,sticky='W')


ttk.Label(monty, text = " Enter a name:").grid(column = 0, row = 0)
name = tk.StringVar()
nameEntered = ttk.Entry(monty, width = 12, textvariable = name)
nameEntered.grid(column = 0, row =1)

action = ttk.Button(monty, text="Click Me !", command = clickMe)
action.grid(column = 2, row =1)

ttk.Label(monty, text ="Choose a number:").grid(column = 1, row = 0)
number = tk.StringVar()
numberChosen = ttk.Combobox(monty, width = 12, textvariable = number, state = 'readonly')
numberChosen['values'] = (1,2,4,42,100)
numberChosen.grid(column = 1, row =1)
numberChosen.current(0)

# Create check button
chVarDis = tk.IntVar()

check1 = tk.Checkbutton(monty, text = "Disabled", variable = chVarDis, state='disabled')
check1.select()
check1.grid(column =0 , row = 4, sticky = tk.W)
chVarUn = tk.IntVar()
check2 = tk.Checkbutton(monty, text ="UnChecked", variable = chVarUn)
check2.deselect()
check2.grid(column = 1, row = 4, sticky = tk.W)

chVarEn = tk.IntVar()
check3 = tk.Checkbutton(monty, text="Enabled", variable = chVarEn)
check3.select()
check3.grid(column = 2, row = 4, sticky = tk.W)
# SpinBox callBack
# Create Spinbox
spin = Spinbox(monty, from_= 0, to=10, width=5, bd=8, command=_spin)
spin.grid(column = 4, row=4)

tt.createToolTip(spin, 'Lựa chọn số này.')
# GUI callBack function

## chVarDis.trace('w', lambda unused0, unused1,unused2: checkCallback())
chVarUn.trace('w', lambda unused0, unused1,unused2: checkCallback())
chVarEn.trace('w', lambda unused0, unused1,unused2: checkCallback())
# Radio Button Globals

color = ["Blue", "Gold", "Red"]
# RadioButton CallsBack
# Creeate three RadioButtons using one variable

radVar = tk.IntVar()
# Next we are selecting a non-exitting index value for radVar

radVar.set(99)

# Now we are creating all three RadioButton widgets winthin one loop
for col in range(3):
    curRad = 'rad' + str(col)
    curRad = tk.Radiobutton(monty, text = color[col], variable = radVar, value = col, command = radCall)
    curRad.grid(column = col, row =6, sticky=tk.W)

# Using a scrolled Text control
scrolW = 40
scrolH = 10

scr = scrolledtext.ScrolledText(monty, width=scrolW, height = scrolH, wrap = tk.WORD)
scr.grid(column = 0, row=5, columnspan = 3)

# Create a containar to hold Labels
labelsFrame = ttk.LabelFrame(monty, text ='Labels in a Frame')
labelsFrame.grid(column = 0, row =7,padx=20, pady=40)

# Place labels into the conteiner element
ttk.Label(labelsFrame, text="Label 1").grid(column =0, row =0 , sticky = tk.W)
ttk.Label(labelsFrame, text="Label 2").grid(column = 0, row = 1, sticky = tk.W)
ttk.Label(labelsFrame, text="label 3").grid(column = 0, row=2, sticky = tk.W)

# Exit Gui cleanly

# Create a Menu bar
menuBar = Menu(win)
win.config(menu=menuBar)

fileMenu = Menu(menuBar, tearoff =0)
fileMenu.add_command(label="New")
fileMenu.add_separator()
fileMenu.add_command(label="Exit", command=_quit)
menuBar.add_cascade(label="File",menu=fileMenu)

# Display a Message Box
# Add another Menu to the Menu Bar and on item
HelpMenu = Menu(menuBar, tearoff=0)
HelpMenu.add_command(label="About", command=_msgBox)
HelpMenu.add_separator()
HelpMenu.add_command(label="Help")
menuBar.add_cascade(label="Help",menu=HelpMenu)

win.iconbitmap(r'E:\python\project\learningGui\icon.ico')
nameEntered.focus()



win.mainloop()