import tkinter as tk
from tkinter import ttk
from tkinter import scrolledtext
from tkinter import Menu
from tkinter import messagebox as m_box
from tkinter import Spinbox
from tkinter import*
import time
from openpyxl import load_workbook
from tkinter import filedialog
import openpyxl
from os import path

# https://www.youtube.com/watch?v=KHjtFNM3Cbk&t=5052s


operator = ""
tp_name = ['Cám', 'Thuốc', 'Điện', 'VT_Sắt', 'VT_Điện',
           'VT_Khác', 'Trứng_Gà', 'Gà_Giống', 'Gà_Thải']
numberdv = ['Kg', 'g', 'Tấn', 'Bao', 'VND', 'lít', 'cái', 'chiếc',
            'Quả', 'Con']



def frame_sofware():
        win = tk.Tk()
        win.title('PHẦN MÊM QUẢN LÝ CHICKEN FARM')
        tab_control = ttk.Notebook(win)
        tabl1 = ttk.Frame(tab_control)
        tab_control.add(tabl1, text='THÔNG SỐ NHẬP VÀO')
        tabl2 = ttk.Frame(tab_control)
        tab_control.add(tabl2, text='THÔNG SỐ TÍNH TOÁN')
        tab_control.pack(expand=1, fill='both')
def frame_tabl1():
        ft1 = frame_sofware()
        tabl2 = ft1.tabl1
        monty = ttk.LabelFrame(tabl2, text='BẢNG SỐ LIỆU')
        monty.grid(column=0, row=1, ipadx=10)
        tieu_de = ttk.LabelFrame(monty)
        tieu_de.grid(column=0, row=0, ipadx=50)
        hien_thi = ttk.LabelFrame(monty)
        hien_thi.grid(column=0, row=1, ipadx=20)
        TP = ttk.LabelFrame(tieu_de)
        TP.grid(column=0, row=0)
        NL = ttk.LabelFrame(tieu_de)
        NL.grid(column=1, row=0)
        SLDV = ttk.LabelFrame(tieu_de)
        SLDV.grid(column=2, row=0)
        GT = ttk.LabelFrame(tieu_de)
        GT.grid(column=3, row=0)
        DV = ttk.LabelFrame(tieu_de)
        DV.grid(column=4, row=0)
        option1 = ttk.LabelFrame(monty)
        option1.grid(column=0, row=2)


def func_time():
        tabl1 = frame_sofware().tabl1
        local_time = time.asctime(time.localtime(time.time()))
        lbltime = Label(tabl1, font=('arial', 20, 'bold'), text=local_time,
                        fg='Steel Blue', bd=10, anchor='w')
        lbltime.grid(column=0, row=0)


def file_link():
    filePath = StringVar()
    try:
        file_name_link = filedialog.askopenfilename(
                      filetypes=(('File', '*.xlsx'), ('All Files', '*.*')))
        filePath.set(file_name_link)
        wb = load_workbook(filePath.get())
    except IOError:
        m_box.showerror('THÔNG BÁO', 'Không tìm thấy file')


def is_float(value):
        try:
            float(value)
            return True
        except ValueError:
            return False


def check_price():
    gia_thanh = input_price().money.get()
    if gia_thanh == '':
        m_box.showwarning('THÔNG BÁO', 'VUI LÒNG NHẬP GIÁ TIỀN')


def check_float():
    gia_thanh = input_price().money.get()
    kt_float = is_float(gia_thanh)
    if kt_float is False:
        m_box.showerror('THÔNG BÁO', 'VUI LÒNG NHẬP SỐ VÀO GIÁ TIỀN')


def check_name_material():
    nguyen_lieu = name_material().name.get()
    if nguyen_lieu == '':
        m_box.showerror('THÔNG BÁO', 'VUI LÒNG NHẬP TÊN SẢN PHẨM')


def tinh_so_luong():
    spin = create_spin().spin
    x = float(spin.get())
    y = float(input_pcs().number.get())
    z = str(x*y)
    return z


def tinh_gia_tien():
    x = float(tinh_so_luong())
    y = float(input_price().money.get())
    z = str(x*y)
    return z


def tinh_toan_gia_thanh():
    numberChosenTP = name_sheet().numberChosenTP
    numberChosendv = input_unit().numberChosendv
    cost_number = tinh_so_luong()
    cost_gia_tri = float(tinh_gia_tien())
    cost_hien_thi = str('%.3f' % cost_gia_tri)
    gia_tri_hien_thi = (numberChosenTP.get() + '    ' + name_material().name.get() + '    ' +
                        cost_number + '   ' + numberChosendv.get() + '   ' +
                        cost_hien_thi + '    ' + 'VND')
    return gia_tri_hien_thi


def write_value_sheet(ten_sheet):
    filePath = file_link().filePath
    scr = create_scrolled().scr
    value1 = tinh_toan_gia_thanh()
    value2 = tinh_so_luong()
    value3 = tinh_gia_tien()
    scr.insert(tk.INSERT, value1 + '\n')
    wb = load_workbook(filePath.get())
    sheetW = wb.get_sheet_by_name(ten_sheet)
    colums = sheetW['B']
    for i in colums:
        if i.value is None:
            x = float(i.row)
            sheetW.cell(row=x, column=2).value = name_material().name.get()
            sheetW.cell(row=x, column=3).value = value2
            sheetW.cell(row=x, column=4).value = value3
            wb.save(filePath.get())
            break


def click_ok():
    ten_sheet = name_sheet().numberChosenTP.get()
    check_price()
    check_float()
    check_name_material()
    write_value_sheet(ten_sheet)


def click_cancel():
    global operator
    operator = ''
    name_material().name.set('')

# Exit Gui cleanly


def _quit():
    win = frame_sofware().win
    win.quit()
    win.destroy()
    exit
# Display a Message Box


def _msgBox():
    m_box.showinfo('PHẦN MÊM QUẢN LÝ CHICKEN FARM',
                   'Sử dụng cho chicken Farm của Hoàn:\n Khởi tạo năm 2018')

 
def icon_path():
    dir = path.dirname(__file__)
    return path.join(dir, 'chicken.ico')
    

def img_path():
        dir = path.dirname(__file__)
        return path.join(dir, 'chicken6.png')


def path_file_excel():
        monty = frame_tabl1().monty
        filePath = file_link().filePath
        labels_frame = ttk.LabelFrame(monty,
                                      text='Nhập đường dẫntới File Excel:')
        labels_frame.grid(column=0, row=7, padx=10, pady=20)
        ttk.Label(labels_frame, text='Link:').grid(column=0,
                                                   row=0, sticky=tk.W)
        Link = ttk.Entry(labels_frame, width=60, textvariable=filePath)
        Link.grid(column=1, row=0)
        btnlink = ttk.Button(labels_frame, width=20, text='Chọn Đường Dẫn',
                             command=file_link)
        btnlink.grid(column=2, row=0)


def name_sheet():
        TP = frame_tabl1().TP
        ttk.Label(TP, text=' Thành Phần:').grid(column=0, row=0)
        numberChosenTP = ttk.Combobox(TP, width=10, values=tp_name,
                                      state='readonly')
        numberChosenTP.grid(column=0, row=1)
        numberChosenTP.current(0)


def name_material():
        name = StringVar()
        NL = frame_tabl1().NL
        ttk.Label(NL, text='Tên Nguyên Liệu:').grid(column=1, row=0)
        name_entered = ttk.Entry(NL, width=30, textvariable=name)
        name_entered.grid(column=1, row=1)
        name_entered.focus()


def input_pcs():
        SLDV = frame_tabl1().SLDV
        number = tk.StringVar()
        ttk.Label(SLDV, text='Số Lượng Đầu Vào:').grid(column=2, row=0)
        number_chosen = ttk.Combobox(SLDV, width=6, textvariable=number,
                                state='readonly')
        number_chosen['values'] = (1, 5, 10, 15, 20, 25, 50, 100, 1000, 2000,
                                3000, 4000, 5000)
        number_chosen.grid(column=2, row=1)
        number_chosen.current(0)


def input_price():
        money = StringVar()
        GT = frame_tabl1().GT
        ttk.Label(GT, text='Giá Thành(VND):').grid(column=3, row=0)
        nameMoney = ttk.Entry(GT, width=12, textvariable=money)
        nameMoney.grid(column=3, row=1)


def input_unit():
        DV = frame_tabl1().DV
        ttk.Label(DV, text='Đơn Vị:').grid(column=6, row=0)
        numberChosendv = ttk.Combobox(DV, width=6, values=numberdv,
                                state='readonly')
        numberChosendv.grid(column=6, row=1)
        numberChosendv.current(0)


def create_spin():
        hien_thi = frame_tabl1().hien_thi
        spin = Spinbox(hien_thi, from_=0, to=10, width=5, bd=8)
        spin.grid(column=5, row=5, sticky=tk.E)


def action_ok():
        tieu_de = frame_tabl1().tieu_de
        action = ttk.Button(tieu_de, text='OK', command=click_ok)
        action.grid(column=8, row=0)


def create_action_cancel():
        tieu_de = frame_tabl1().tieu_de
        action_cancel = ttk.Button(tieu_de, text='Cancel', command=click_cancel)
        action_cancel.grid(column=9, row=0)


def create_scrolled():
        hien_thi = frame_tabl1().hien_thi
        scrol_w = 60
        scrol_h = 20
        scr = scrolledtext.ScrolledText(hien_thi, width=scrol_w, height=scrol_h,
                                        wrap=tk.WORD)
        scr.grid(column=0, row=5, columnspan=3)


def create_menu_bar():
        win = frame_sofware().win
        menu_bar = Menu(win)
        win.config(menu=menu_bar)
        file_menu = Menu(menu_bar, tearoff=0)
        file_menu.add_command(label='New')
        file_menu.add_separator()
        file_menu.add_command(label='Exit', command=_quit)
        menu_bar.add_cascade(label='File', menu=file_menu)
        # Add another Menu to the Menu Bar and on item
        help_menu = Menu(menu_bar, tearoff=0)
        help_menu.add_command(label='About', command=_msgBox)
        help_menu.add_separator()
        help_menu.add_command(label='Help')
        menu_bar.add_cascade(label="Help", menu=help_menu)


def create_image():
        frame_sofware().win.iconbitmap(icon_path())
        chicken_photo = PhotoImage(file=img_path())
        Label(frame_tabl1().hien_thi, image=chicken_photo).grid(column=8,
                                                                row=5, sticky=tk.SE)


def main():
        frame_sofware()
        frame_tabl1()
        create_menu_bar()
        create_image()
        func_time()
        path_file_excel()
        name_sheet()
        name_material()
        input_pcs()
        input_price()
        input_unit()
        create_spin()
        action_ok()
        create_action_cancel()
        create_scrolled()

if __name__=='__main__':
        main()

frame_sofware().win.mainloop()
