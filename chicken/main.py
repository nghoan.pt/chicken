from os import path, getcwd

cwd = getcwd()
rel_path = 'abc/def'

print('cwd:', cwd)
print('current file (relative):', __file__)
print('current file (absolute):', path.abspath(__file__))

print('relative: ', rel_path)
print('cwd + rel_path: ', path.join(cwd, rel_path))
