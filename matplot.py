import matplotlib.pyplot as plt
import numpy as np
x =[]
y =[]
plt.grid(True)
for i in np.arange(0, 2*np.pi, 2*np.pi/50):
    x.append(i)
    y.append(np.sin(i))

plt.plot(x,y,'bo-')
plt.plot(x,y,'r-',linewidth=2)
plt.axis([0,2*np.pi,-1.5,1.5])
plt.title('Bieu do nhiet do')
plt.xlabel('Thoi gian')
plt.ylabel('Nhiet do')
plt.show()